<?php /** @var $l OC_L10N */ ?>
<?php
vendor_script('jsTimezoneDetect/jstz');
script('core', [
    'visitortimezone',
    'lostpassword',
    'login'
]);
?>
<?php

// nombre de dossiers utilisateurs présents
// dans le dossier /data (sans compter admin et updater_backup)
// rajout de 2000 utilisateurs artificiels correspondant aux ~ 2k utilisateurs désactivés (15/12/16)
$currentAccounts = count(glob($_SERVER['DOCUMENT_ROOT'].'/data/*',GLOB_ONLYDIR));

// comptes disponibles
$leftAccounts = 7000 - $currentAccounts;

$nbJours = (strtotime(date("Y-m-d")) - strtotime("2016-12-14"))/(60*60*24);
// nombre de comptes maximum de l'instance
/*
if ($nbJours < 1) {
    $maxAccounts = $currentAccounts;
} elseif ($nbJours < 40) {
    $maxAccounts = 5650 - 50*(40-$nbJours);
} else {
    $maxAccounts = 5650;
}*/

// nombre de comptes disponibles
$freeAccounts = $leftAccounts - 50*(35-$nbJours);
?>
<div class="col-md-8 text-center" id="classic">
    <p><img src="themes/framadrive/core/img/framadrive.png" alt="" class="ombre screenshot" /></p>

<?php if (!empty($_['alt_login']) && $freeAccounts > 0 ) { ?>
<form id="alternative-logins">
    <fieldset>
        <legend><?php p($l->t('Alternative Logins')) ?></legend>
        <ul>
            <?php foreach($_['alt_login'] as $login): ?>
                <li><a class="btn btn-lg btn-success center-block" href="<?php print_unescaped($login['href']); ?>" ><i class="glyphicon glyphicon-user"></i> <?php p($login['name']); ?></a></li>
            <?php endforeach; ?>
        </ul>
    </fieldset>
</form>
    <div class="col-sm-10 col-sm-offset-1 alert alert-info">
        <p>Le nombre de comptes est limité à 5000 pour le moment.<br/>
           Il reste <strong><?php echo $freeAccounts ?> places disponibles aujourd'hui</strong>.
        </p>
    </div>
<?php } else { ?>
    <div class="col-sm-10 col-sm-offset-1 alert alert-warning">
        <p class="text-left">Le nombre maximal de comptes (5000) a été atteint.</p>
        <p class="text-left">Nous n’ouvrirons pas de comptes au delà. Nous vous invitons à vous renseigner auprès d’autres hébergeurs comme les <a href="https://chatons.org/">CHATONS</a>, par exemple <a href="https://indie.host/fr/">notre partenaire IndieHosters</a>.</p>
    </div>
<?php } ?>

</div>
<div class="col-md-4" id="presentation">
    <h2>Se connecter</h2>

<!--[if IE 8]><style>input[type="checkbox"]{padding:0;}</style><![endif]-->
<form method="post" name="login">
    <fieldset>
    <?php if (!empty($_['redirect_url'])) {
        print_unescaped('<input type="hidden" name="redirect_url" value="' . OC_Util::sanitizeHTML($_['redirect_url']) . '">');
    } ?>
        <?php if (isset($_['apacheauthfailed']) && ($_['apacheauthfailed'])): ?>
            <div class="warning">
                <?php p($l->t('Server side authentication failed!')); ?><br>
                <small><?php p($l->t('Please contact your administrator.')); ?></small>
            </div>
        <?php endif; ?>
        <?php foreach($_['messages'] as $message): ?>
            <div class="warning">
                <?php p($message); ?><br>
            </div>
        <?php endforeach; ?>
        <?php if (isset($_['internalexception']) && ($_['internalexception'])): ?>
            <div class="warning">
                <?php p($l->t('An internal error occured.')); ?><br>
                <small><?php p($l->t('Please try again or contact your administrator.')); ?></small>
            </div>
        <?php endif; ?>
        <div id="message" class="hidden">
            <img class="float-spinner" alt=""
                src="<?php p(\OCP\Util::imagePath('core', 'loading-dark.gif'));?>">
            <span id="messageText"></span>
            <!-- the following div ensures that the spinner is always inside the #message div -->
            <div style="clear: both;"></div>
        </div>
        <p class="grouptop">
            <input type="text" name="user" id="user"
                placeholder="<?php p($l->t('Username')); ?>"
                value="<?php p($_['username']); ?>"
                <?php p($_['user_autofocus'] ? 'autofocus' : ''); ?>
                autocomplete="on" autocapitalize="off" autocorrect="off" required>
            <label for="user" class="infield"><?php p($l->t('Username')); ?></label>
            <img class="svg" src="<?php print_unescaped(image_path('', 'actions/user.svg')); ?>" alt=""/>
        </p>

        <p class="groupbottom">
            <input type="password" name="password" id="password" value=""
                placeholder="<?php p($l->t('Password')); ?>"
                <?php p($_['user_autofocus'] ? '' : 'autofocus'); ?>
                autocomplete="on" autocapitalize="off" autocorrect="off" required>
            <label for="password" class="infield"><?php p($l->t('Password')); ?></label>
            <img class="svg" id="password-icon" src="<?php print_unescaped(image_path('', 'actions/password.svg')); ?>" alt=""/>
            <input type="submit" id="submit" class="login primary icon-confirm svg" title="<?php p($l->t('Log in')); ?>" value="" disabled="disabled"/>
        </p>

        <?php if (isset($_['invalidpassword']) && ($_['invalidpassword'])): ?>
        <a id="lost-password" class="warning" href="">
            <?php p($l->t('Wrong password. Reset it?')); ?>
        </a>
        <?php endif; ?>
        <?php if ($_['rememberLoginAllowed'] === true) : ?>
        <div class="remember-login-container">
            <input type="checkbox" name="remember_login" value="1" id="remember_login" class="checkbox checkbox--white">
            <label for="remember_login"><?php p($l->t('remember')); ?></label><br/>
            <a class="bg-primary" href="#TutoSync" data-toggle="modal" title="Comment synchroniser ses données ?">
                <i class="fa fa-fw fa-refresh"></i> Télécharger l’application
            </a>
        </a>
        </div>
        <?php endif; ?>
        <input type="hidden" name="timezone-offset" id="timezone-offset"/>
        <input type="hidden" name="timezone" id="timezone"/>
        <input type="hidden" name="requesttoken" value="<?php p($_['requesttoken']) ?>">
    </fieldset>
</form>

    <h2>Qu’est ce que c’est ?</h2>
    <p><b class="frama">Frama</b><b class="vert">drive</b> est un service en ligne d’hébergement de fichiers.</p>
    <p>En créant un compte, vous disposez de :</p>
    <ul>
        <li><strong>2Go de stockage</strong> en ligne</li>
        <li><strong>synchronisés</strong> entre vos ordinateurs, tablettes, mobiles… <a href="#TutoSync" data-toggle="modal" class="small" title="Comment synchroniser ses données ?">(comment faire ?)</a></li>
        <li>et que <strong>vous pouvez partager</strong> facilement avec vos contacts.</li>
    </ul>
    <p>Pour obtenir plus d’espace, vous pouvez vous adresser à
       <a href="https://indie.host/fr/">IndieHosters</a>, signataire de notre
       <a href="https://framasoft.org/nav/html/charte.html">charte</a>.<br/>
       Vous trouverez aussi une liste d’<a href="https://owncloud.org/providers/">hébergeurs sur le site d’OwnCloud</a>.
    </p>

    <!-- modale TutoSync -->
    <div class="modal fade" id="TutoSync" tabindex="-1" role="dialog" aria-labelledby="TutoSyncLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">x</span><span class="sr-only">Fermer</span></button>
                    <h1 id="TutoSyncLabel">Synchroniser ses données</h1>
                </div>

                <div class="modal-body clearfix">
                    <p>Pour synchroniser vos données sur plusieurs appareils,
                    il faut télécharger le client Owncloud de synchronisation correspondant à votre environnement.</p>

                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active" id="li-tab-desktop">
                            <a href="#tab-desktop" class="btn btn-lg" role="tab" data-toggle="tab">
                                <i class="fa fa-fw fa-lg fa-desktop"></i> Client pour PC et Mac</a>
                        </li>
                        <li id="li-tab-mobile">
                            <a href="#tab-mobile" class="btn btn-lg" role="tab" data-toggle="tab">
                                <i class="fa fa-fw fa-lg fa-mobile"></i> Application pour mobiles et tablettes</a>
                        </li>
                    </ul>
                    <br>
                    <div class="tab-content col-xs-12">
                        <div id="tab-desktop" role="tabpanel" class="tab-pane active">
                            <div class="">
                                <h2 class="h4">Télécharger</h2>
                                <p><strong>Le client de synchronisation</strong> existe pour Windows, Mac OS et GNU/Linux.</p>
                                <p class="text-center">
                                    <a href="https://download.owncloud.com/desktop/stable/ownCloud-2.2.4.6408-setup.exe" class="btn btn-lg btn-default">
                                        <i class="fa fa-fw fa-lg fa-windows"></i> Windows<br>
                                    </a>
                                    <a href="https://download.owncloud.com/desktop/stable/ownCloud-2.2.4.3709.pkg" class="btn btn-lg btn-default">
                                        <i class="fa fa-fw fa-lg fa-apple"></i> Mac OS<br>
                                    </a>
                                    <a target="_blank" href="https://software.opensuse.org/download/package?project=isv:ownCloud:desktop&amp;package=owncloud-client" class="btn btn-lg btn-default">
                                        <i class="fa fa-fw fa-lg fa-linux"></i> GNU/Linux<br>
                                    </a>
                                </p>
                                <hr role="presentation" />
                                <h2 class="h4">Configurer</h2>
                                <p>Une fois le client installé et lancé, une fenêtre s’ouvre pour vous aider à configurer le client.</p>
                                <p>Saisissez l'adresse sur serveur : <code>https://framadrive.org</code></p>
                                <p class="text-center"><img src="/themes/framadrive/core/img/dsync-server.jpg" /></p>
                                <p>Puis, votre nom d'utilisateur et votre mot de passe.</p>
                                <p class="text-center"><img src="/themes/framadrive/core/img/dsync-login.jpg" /></p>
                                <p>Choisissez les dossiers que vous voulez voir synchronisés.</p>
                                <p class="text-center"><img src="/themes/framadrive/core/img/dsync-folders.jpg" /></p>
                                <p>Enfin, une icône en forme de nuage dans la zone de notification vous indique que la synchronisation a commencé.
                                En cliquant dessus vous pouvez voir la listes des fichiers transférés</p>
                                <p class="text-center"><img src="/themes/framadrive/core/img/dsync-end.jpg" /></p>
                            </div>
                        </div>
                        <div id="tab-mobile" role="tabpanel" class="tab-pane">
                            <p><strong>L’application mobile</strong> existe sur F-Droid, l’Apple Store, Google Play…</p>
                            <div class="text-center">
                                <a href="https://f-droid.org/repository/browse/?fdfilter=owncloud&fdid=com.owncloud.android"
                                    target="_blank" class="col-sm-6" >
                                    <img src="/themes/framadrive/core/img/fdroid.png">
                                </a>
                                <a href="https://itunes.apple.com/us/app/owncloud/id543672169?ls=1&amp;mt=8"
                                    target="_blank" class="col-sm-6" >
                                    <img src="https://owncloud.org/wp-content/themes/owncloudorgnew/assets/img/clients/buttons/appstore.png">
                                </a>
                                <a href="https://play.google.com/store/apps/details?id=com.owncloud.android"
                                    target="_blank" class="col-sm-6" >
                                    <img src="https://owncloud.org/wp-content/themes/owncloudorgnew/assets/img/clients/buttons/googleplay.png">
                                </a>
                                <a href="https://appworld.blackberry.com/webstore/content/59955931/"
                                    target="_blank" class="col-sm-6" >
                                    <img src="https://owncloud.org/wp-content/themes/owncloudorgnew/assets/img/clients/buttons/blackberry.png">
                                </a>
                                <a href="https://www.amazon.com/ownCloud-Inc/dp/B00944PQMK"
                                    target="_blank" class="col-sm-6" >
                                    <img src="https://owncloud.org/wp-content/themes/owncloudorgnew/assets/img/clients/buttons/amazon-store.png">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer"><a href="#" class="btn btn-default" data-dismiss="modal">Fermer</a></div>
            </div>
        </div>
    </div>
    <!-- /modale TutoSync -->

</div>
</div> <!-- .row -->
<hr role="presentation" />
<div class="row">
    <div class="col-md-4" id="tuto-video">
        <h2>Tutoriel vidéo</h2>
        <p class="text-center" role="presentation"><span class="glyphicon glyphicon-film"></span></p>
        <p>Pour vous aider dans l’utilisation du logiciel, voici un tutoriel vidéo réalisé par <a href="http://arpinux.org/">arpinux</a>, artisan paysagiste de la distribution GNU/Linux pour débutant <a href="https://handylinux.org/">HandyLinux</a>.</p>
        <p class="text-center"><a href="#TutoVideo" data-toggle="modal" class="btn btn-primary">Lire la vidéo »</a></p>
   </div>

    <!-- modale vidéo -->
    <div class="modal fade" id="TutoVideo" tabindex="-1" role="dialog" aria-labelledby="TutoVideoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">x</span><span class="sr-only">Fermer</span></button>
                    <h1 id="TutoVideoLabel">Tutoriel vidéo</h1>
                </div>
                <div class="modal-body">
                    <p><video width="570" height="340" controls="controls" preload="none" poster="https://framatube.org/images/media/974l.jpg">
                        <source src="https://framatube.org/blip/framadrive.mp4" type="video/mp4" />
                        <source src="https://framatube.org/blip/framadrive.webm" type="video/webm" />
                    </video></p>
                    <p>-> La <a href="https://framatube.org/blip/framadrive.webm">vidéo au format webm</a></p>
               </div>
               <div class="modal-footer"><a href="#" class="btn btn-default" data-dismiss="modal">Fermer</a></div>
           </div>
        </div>
    </div>
    <!-- /modale vidéo -->

    <div class="col-md-4" id="le-logiciel">
        <h2>Le logiciel</h2>
        <p class="text-center" role="presentation"><span class="glyphicon glyphicon-cloud"></span></p>
        <p><b class="frama">Frama</b><b class="vert">drive</b> repose sur le logiciel libre
        <a href="https://owncloud.org">Owncloud</a>.
        Les données sont hébergées sur les serveurs de <b class="frama">Frama</b><b class="soft">soft</b><br/>
        Il s’agit d’<a href="https://nextcloud.com/providers/">une instance parmi d'autres</a>.</p>
        <p>Nextcloud est sous <a href="https://github.com/nextcloud/server/blob/master/COPYING-README">licence <abbr title="Affero General Public License">AGPL</abbr></a>.</p>
        <p>Pour synchroniser vos documents sur vos ordinateurs, tablettes ou mobiles,
        il est nécessaire d’installer <a href="#TutoSync" data-toggle="modal" >le client Owncloud</a>
        correspondant à votre environnement.</p>

    </div>

    <div class="col-md-4" id="jardin">
        <h2>Cultivez votre jardin</h2>
        <p class="text-center" role="presentation"><span class="glyphicon glyphicon-tree-deciduous"></span></p>
        <p>Pour participer au développement du logiciel, proposer des améliorations
            ou simplement le télécharger, rendez-vous sur <a href="https://github.com/owncloud/">le site de développement</a>.</p>
        <br>
        <p>Si vous souhaitez installer ce logiciel pour votre propre usage et ainsi gagner en autonomie, nous vous aidons sur :</p>
        <p class="text-center"><a href="http://framacloud.org/cultiver-son-jardin/installation-de-owncloud/" class="btn btn-success"><span class="glyphicon glyphicon-tree-deciduous"></span> framacloud.org</a></p>
    </div>
</div>
